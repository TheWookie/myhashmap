package myhash;

import java.util.Arrays;

public class MyHashMap<K, V> {
	public static final int ADDED_MEMORY = 10;
	public static final int INITIAL_MEMORY = 16;
	public static final double INITIAL_LOAD_FACTOR = 0.75;
	
	static class Entry<K, V> {
		K key;
		V value;
		
		public Entry(K key, V value) {
			this.key = key;
			this.value = value;
		}
		
		public K getKey() {
			return key;
		}

		public V getValue() {
			return value;
		}
		
		public void setValue(V value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return "Entry [key=" + key + ", value=" + value + "]";
		}

	}
	
	private Entry<K, V> entries[];
	private int size = 0;
	private double loadFactor;
	
	public MyHashMap() {
		this.entries = new Entry[INITIAL_MEMORY];
		loadFactor = INITIAL_LOAD_FACTOR;
	}
	
	public MyHashMap(int initialMemory) {
		this.entries = new Entry[initialMemory];
		loadFactor = INITIAL_LOAD_FACTOR;
	}
	
	public MyHashMap(int initialMemory, double initialLoadFactor) {
		this.entries = new Entry[initialMemory];
		loadFactor = initialLoadFactor;
	}
	
	private int hash(K key) {
		int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
	}
	
	private int calculateIndexWithHash(K key) {
		return hash(key) % entries.length;
	}
	
	private int findFreeIndex(int index) {
		for(int i = index + 1; i < entries.length; i++) {
			if(entries[i] == null)
				return i;
		}
		
		for(int i = 0; i < index; i++) {
			if(entries[i] == null)
				return i;
		}
		
		return 0;
	}
	
	private void allocateSpace() {
		Entry<K, V> temp[] = new Entry[entries.length + ADDED_MEMORY];
        System.arraycopy(entries, 0, temp, 0, entries.length);
        entries = temp;
	}
	
	private void checkCapacity() {
		if(size/entries.length > loadFactor)
			allocateSpace();
	}
	
	public Entry<K, V> put(K key, V value) {
		if(key == null)
			throw new IllegalArgumentException("Key is null");
		
		checkCapacity();
		
		int index = calculateIndexWithHash(key);
		Entry<K, V> newEntry = new Entry<>(key, value);
		
		if(entries[index] != null) {
			if(key.equals(entries[index].getKey())) {
				entries[index].setValue(value);
				return newEntry;
			} else {
				index = findFreeIndex(index);
			}	
		}
		
		entries[index] = newEntry;
		
		size++;
		return newEntry;
	}
	
	public V get(K key) {
		if(key == null)
			throw new IllegalArgumentException("Key is null");
		
		for(Entry<K, V> entry : entries) {
			if(entry != null)
				if(hash(key) == hash(entry.getKey()))
					if(key.equals(entry.getKey()))
						return entry.getValue();
		}
		
		return null;
	}
	
	public int size() {
		return size;
	}

	@Override
	public String toString() {
		return "MyHashMap [entries=" + Arrays.toString(entries) + "]";
	}
	
}
