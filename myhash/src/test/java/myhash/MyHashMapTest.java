package myhash;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;

public class MyHashMapTest {
	MyHashMap<Integer, Long> map;
	
	@Before
	public void beforeEach() {
		map = new MyHashMap<>();
	}
	
	@Test
	public void testPutValuesWithSameKeys() {
		map.put(1, (long) 1);
		map.put(1, (long) 2);
		map.put(4, (long) 3);
		
		assertEquals(2, map.size());
		assertEquals(new Long(2), map.get(1));
	}
	
	@Test
	public void testPutKeysWithSameHash() {
		class TestClass {
			public int hashCode() {
				return 1;
			}
		}
		
		MyHashMap<Object, Long> objMap = new MyHashMap<>();
		
		TestClass i1 = new TestClass();
		TestClass i2 = new TestClass();
		
		objMap.put(i1, (long) 1);
		objMap.put(i2, (long) 2);
		
		assertEquals(2, objMap.size());
		assertEquals(new Long(1), objMap.get(i1));
		assertEquals(new Long(2), objMap.get(i2));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testPutNullKey() {
		map.put(null, (long) 10);
	}
	
	@Test
	public void testGetInexistentKey() {
		map.put(1, (long) 100);
		
		assertNull(map.get(2));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetNullKey() {
		map.get(null);
	}
	
	@Test
	public void testPutWithIncreasingMapSize() {
		int count = 20;
		
		for(int i = 0; i < count; i++) {
			map.put(i, (long) i);
		}
		
		assertEquals(count, map.size());
		assertEquals(new Long(0), map.get(0));
		assertEquals(new Long(1), map.get(1));
		assertEquals(new Long(10), map.get(10));
		assertEquals(new Long(19), map.get(19));
	}
	
}
